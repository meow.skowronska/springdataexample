package edu.ib.springdataexample.product;

public class ProductDto {
    private Long id;

    private String name;
    private Float price;
    private Boolean available;

    public ProductDto() {
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", available=" + available +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public ProductDto(Long id, String name, float price, boolean available) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.available = available;
    }
}
