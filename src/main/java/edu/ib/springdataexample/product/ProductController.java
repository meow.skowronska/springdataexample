package edu.ib.springdataexample.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/all")
    public List<ProductDto> findAll() {
        return productService.getAll();
    }

    @GetMapping("/product")
    public ProductDto getById(@RequestParam Long id) {
        return productService.getById(id);
    }

    @PostMapping("/admin/product")
    public List<ProductDto> add(@RequestBody ProductDto productDto) {
        return productService.save(productDto);
    }

    @PutMapping("/admin/product")
    public List<ProductDto> modify(@RequestParam Long id, @RequestBody ProductDto productDto) {
        return productService.save(productDto, id);
    }

    @PatchMapping("/admin/product")
    public List<ProductDto> partModify(@RequestParam Long id,  @RequestBody ProductDto productDto) {
        return   productService.update(productDto, id);
    }

}

