package edu.ib.springdataexample.product;

import edu.ib.springdataexample.product.Product;
import edu.ib.springdataexample.product.ProductDto;
import edu.ib.springdataexample.product.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service

public class ProductService {
    private final ProductRepo productRepo;
@Autowired
    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }



    public List<ProductDto> getAll(){
    List<Product> list= (List<Product>) productRepo.findAll();
        return  list.stream().map(p->new ProductDto(p.getId(),p.getName(),p.getPrice(),p.isAvailable())).collect(Collectors.toList());
}


    public List<ProductDto> save(ProductDto productDto){
        Product product=new Product(productDto.getId(),productDto.getName(),productDto.getPrice(),productDto.isAvailable());
        productRepo.save(product);
        return getAll();
    }


    public ProductDto getById(Long id) {
    Product product= productRepo.findById(id).get();
    return new ProductDto(product.getId(),product.getName(),product.getPrice(),product.isAvailable());
    }

    public List<ProductDto> save(ProductDto productDto, Long id) {
        Product product=new Product(id,productDto.getName(),productDto.getPrice(),productDto.isAvailable());
        productRepo.save(product);
        return  getAll();
    }

    public List<ProductDto> update(ProductDto productDto, Long id) {
        System.out.println(productDto);

        Product product= productRepo.findById(id).get();
        if(productDto.getName()!=null){
            System.out.println("1");
            product.setName(productDto.getName());
        }if(productDto.isAvailable()!=null){
            System.out.println("2");

            product.setAvailable(productDto.isAvailable());
        }if(productDto.getPrice()!=null){
            System.out.println("3");

            product.setPrice(productDto.getPrice());
        }
        productRepo.save(product);
return getAll();
    }
}
