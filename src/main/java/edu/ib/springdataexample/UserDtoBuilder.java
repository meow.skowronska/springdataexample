package edu.ib.springdataexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserDtoBuilder {
    private UserRepo userRepo;

    @Autowired
    public UserDtoBuilder(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
     UserDto userDto1=new UserDto("ola","haslo","ROLE_CUSTOMER");
     UserDto userDto2=new UserDto("adam","haslo","ROLE_CUSTOMER");
     UserDto userDto3=new UserDto("hania","haslo","ROLE_ADMIN");
     UserDto userDto4=new UserDto("gosia","haslo","ROLE_ADMIN");
    userRepo.save(map(userDto1));
    userRepo.save(map(userDto2));
    userRepo.save(map(userDto3));
    userRepo.save(map(userDto4));
    }
    @GetMapping("/all")
    public List<User> findAll() {
        return (List<User>) userRepo.findAll();
    }
    @GetMapping()
    public User findById(@RequestParam Long id) {
        return userRepo.findById(id).get();
    }
    private User map(UserDto userDto){
        return new User(userDto.getName(),userDto.getPasswordHash(),userDto.getRole());
    }
}
