package edu.ib.springdataexample;

import edu.ib.springdataexample.customer.Customer;
import edu.ib.springdataexample.customer.CustomerRepo;
import edu.ib.springdataexample.order.Order;
import edu.ib.springdataexample.order.OrderRepo;
import edu.ib.springdataexample.product.Product;
import edu.ib.springdataexample.product.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {

    private ProductRepo productRepo;
    private OrderRepo orderRepo;
    private CustomerRepo customerRepo;

    @Autowired
    public DbMockData(ProductRepo productRepo, OrderRepo orderRepo, CustomerRepo customerRepo) {
        this.productRepo = productRepo;
        this.orderRepo = orderRepo;
        this.customerRepo = customerRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {

        Product product1 = new Product("Box", 10, true);
        Product product2 = new Product("Cat", 52, true);
        Product product3 = new Product("Horse", 1214, false);

        Customer customer1 = new Customer("Jan Nowak", "Wrocław szybka");
        Set<Product> products1 = new HashSet() {
            {
                add(product1);
                add(product2);
                add(product3);
            }};

        Order order = new Order(customer1,products1, LocalDateTime.now(),"deliverd");

        productRepo.save(product1);
        productRepo.save(product2);
        productRepo.save(product3);
        customerRepo.save(customer1);
        orderRepo.save(order);


    }



}
