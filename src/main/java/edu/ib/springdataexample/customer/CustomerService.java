package edu.ib.springdataexample.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private final CustomerRepo customerRepo;
@Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public List<CustomerDto> getAll(){
        List<Customer> list= (List<Customer>) customerRepo.findAll();
        return  list.stream().map(c->map(c)).collect(Collectors.toList());
    }


    public List<CustomerDto> save(CustomerDto customerDto){
       customerRepo.save(map(customerDto));
       return getAll();
    }


    public CustomerDto getById(Long id) {
        Customer customer= customerRepo.findById(id).get();
        return map(customer);
    }

    public List<CustomerDto> save(CustomerDto customerDto, Long id) {
        customerDto.setId(id);
        customerRepo.save(map(customerDto));
        return getAll();
    }

    public List<CustomerDto> update(CustomerDto customerDto, Long id) {
        Customer customer= customerRepo.findById(id).get();
        System.out.println(customerDto);
        System.out.println(customer);
        if(customerDto.getName()!=null){
            customer.setName(customerDto.getName());
        }if(customerDto.getAddress()!=null) {
            customer.setAddress(customerDto.getAddress());
        }
        customerRepo.save(customer);
        return getAll();

    }
    public Customer map(CustomerDto c){
    return new Customer(c.getId(),c.getName(),c.getAddress());
    }
    public CustomerDto map(Customer c){
        return new CustomerDto(c.getId(),c.getName(),c.getAddress());
    }
}
