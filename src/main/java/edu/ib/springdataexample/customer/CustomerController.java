package edu.ib.springdataexample.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")

public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/all")
    public List<CustomerDto> findAll() {
        return customerService.getAll();
    }
    @GetMapping("/customer")
    public CustomerDto getById(@RequestParam Long id){
        return customerService.getById(id);
    }

    @PostMapping("/admin/customer")
    public List<CustomerDto> add(@RequestBody CustomerDto customerDto) {
      return   customerService.save(customerDto);
    }

    @PutMapping("/admin/customer")
    public List<CustomerDto> modify(@RequestParam Long id, @RequestBody CustomerDto customerDto) {
        return customerService.save(customerDto,id);
    }

    @PatchMapping("/admin/customer")
    public List<CustomerDto>  partModify(@RequestParam Long id, @RequestBody CustomerDto customerDto){
        System.out.println("dziala");
        System.out.println(customerDto);
        return customerService.update(customerDto,id);

    }}
