package edu.ib.springdataexample.order;

import edu.ib.springdataexample.product.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")

public class OrderController {
    private final OrderService orderService;
@Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }
    @GetMapping("/order/all")
    public List<OrderDto> findAll() {
        return orderService.getAll();
    }
    @GetMapping("/order")
    public OrderDto getById(@RequestParam Long id){
        return orderService.getById(id);
    }

    @PostMapping("/admin/order")
    public List<OrderDto> add(@RequestBody OrderDto orderDto) {
        return orderService.save(orderDto);
    }

    @PutMapping("/admin/order")
    public List<OrderDto> modify(@RequestParam Long id, @RequestBody OrderDto orderDto) {
        return orderService.save(orderDto,id);
    }

    @PatchMapping("/admin/order")
    public List<OrderDto>  partModify(@RequestParam Long id, @RequestBody OrderDto orderDto){
        return   orderService.update(orderDto,id);
    }

}
