package edu.ib.springdataexample.order;



import edu.ib.springdataexample.product.Product;
import edu.ib.springdataexample.customer.Customer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
    @ManyToOne
    private Customer customer;
    @ManyToMany
    private Set<Product> productSet;
    private LocalDateTime placeDate;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<Product> getProductSet() {
        return productSet;
    }

    public void setProductSet(Set<Product> productSet) {
        this.productSet = productSet;
    }

    public LocalDateTime getPlaceDate() {
        return placeDate;
    }

    public void setPlaceDate(LocalDateTime placeDate) {
        this.placeDate = placeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Order() {
    }

    public Order(Customer customer, Set<Product> productSet, LocalDateTime placeDate, String status) {
        this.customer = customer;
        this.productSet = productSet;
        this.placeDate = placeDate;
        this.status = status;
    }

    public Order(Long id, Customer customer, Set<Product> productSet, LocalDateTime placeDate, String status) {
        this.id = id;
        this.customer = customer;
        this.productSet = productSet;
        this.placeDate = placeDate;
        this.status = status;
    }
}
