package edu.ib.springdataexample.order;

import edu.ib.springdataexample.product.Product;
import edu.ib.springdataexample.product.ProductDto;
import edu.ib.springdataexample.customer.Customer;
import edu.ib.springdataexample.customer.CustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service

public class OrderService {
    private final OrderRepo orderRepo;

    @Autowired
    public OrderService(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    public List<OrderDto> getAll() {
        List<Order> list = (List<Order>) orderRepo.findAll();
        List<OrderDto> listdto = list.stream().map(o -> map(o)).collect(Collectors.toList());
        return listdto;


    }

    public List<OrderDto> save(OrderDto orderDto) {
           orderRepo.save(map(orderDto));
           return getAll();
    }

    public OrderDto getById(Long id) {
        Order order = orderRepo.findById(id).get();
        return map(order);
    }

    public OrderDto map(Order o) {
        return new OrderDto(o.getId(), new CustomerDto(o.getCustomer().getId(), o.getCustomer().getName(), o.getCustomer().getAddress()), o.getProductSet().stream().map(p -> new ProductDto(p.getId(), p.getName(), p.getPrice(), p.isAvailable())).collect(Collectors.toList()), o.getPlaceDate(), o.getStatus());
    }

    public Order map(OrderDto o) {
        return new Order(o.getId(), new Customer(o.getCustomerDto().getId(), o.getCustomerDto().getName(), o.getCustomerDto().getAddress()), o.getProductSet().stream().map(p -> new Product(p.getId(), p.getName(), p.getPrice(), p.isAvailable())).collect(Collectors.toSet()), o.getPlaceDate(), o.getStatus());
    }



    public List<OrderDto> update(OrderDto orderDto, Long id) {
        Order order= orderRepo.findById(id).get();
        if(orderDto.getCustomerDto()!=null){
            order.setCustomer(new Customer( orderDto.getCustomerDto().getId(),orderDto.getCustomerDto().getName(),orderDto.getCustomerDto().getAddress()));
        }if(orderDto.getPlaceDate()!=null){
            order.setPlaceDate(orderDto.getPlaceDate());
        }if(orderDto.getStatus()!=null){
           order.setStatus(orderDto.getStatus());
        }if(orderDto.getProductSet()!=null){
            order.setProductSet(orderDto.getProductSet().stream().map(p -> new Product(p.getId(), p.getName(), p.getPrice(), p.isAvailable())).collect(Collectors.toSet()));
        }
      orderRepo.save(order);
return getAll();
    }

    public List<OrderDto> save(OrderDto orderDto, Long id) {
       orderDto.setId(id);
        orderRepo.save(map(orderDto));
        return getAll();
    }
}
