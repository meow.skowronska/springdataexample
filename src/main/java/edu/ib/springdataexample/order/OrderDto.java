package edu.ib.springdataexample.order;


import edu.ib.springdataexample.product.ProductDto;
import edu.ib.springdataexample.customer.CustomerDto;

import java.time.LocalDateTime;
import java.util.List;

public class OrderDto {
    private Long id;
    private CustomerDto customerDto;
    private List<ProductDto> productSet;
    private LocalDateTime placeDate;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerDto getCustomerDto() {
        return customerDto;
    }

    @Override
    public String toString() {
        return "OrderDto{" +
                "id=" + id +
                ", customerDto=" + customerDto +
                ", productSet=" + productSet +
                ", placeDate=" + placeDate +
                ", status='" + status + '\'' +
                '}';
    }

    public void setCustomerDto(CustomerDto customerDto) {
        this.customerDto = customerDto;
    }

    public List<ProductDto> getProductSet() {
        return productSet;
    }

    public void setProductSet(List<ProductDto> productSet) {
        this.productSet = productSet;
    }

    public LocalDateTime getPlaceDate() {
        return placeDate;
    }

    public void setPlaceDate(LocalDateTime placeDate) {
        this.placeDate = placeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OrderDto(Long id, CustomerDto customerDto, List productSet, LocalDateTime placeDate, String status) {
        this.id = id;
        this.customerDto = customerDto;
        this.productSet = productSet;
        this.placeDate = placeDate;
        this.status = status;
    }

    public OrderDto() {

    }
}
