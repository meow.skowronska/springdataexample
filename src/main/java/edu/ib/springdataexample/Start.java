package edu.ib.springdataexample;

import edu.ib.springdataexample.product.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {




    @EventListener(ApplicationReadyEvent.class)
    public void runExample(){

    }

}
